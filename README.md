# Hello World Workshop: AWS Serverless Application with .NET Core

This workshop will deploy a very simple "Hello World" C# application to AWS using Lambda. The Lambda is a basic C# function that will be called whenever a certain HTTP trigger is hit (by sending a HTTP GET request to a certain URL). The Lambda requires a role to execute which covers the basic permissions it will need. The Lambda will log results to CloudWatch.

This repository is a complete example of how the .NET Core Lambda, the API Gateway, and the custom domain name will be deployed using AWS CDK. The details on how this example is put together are in Confluence: [Workshop 01: Minimal .NET Core API Deployment](https://headspring.atlassian.net/wiki/spaces/WISDOM/pages/1453492189/Workshop+01+Minimal+.NET+Core+API+Deployment).

## Pre-requisites

For now, I assume you're using your Headspring AWS Sandbox Account. You can run this from any AWS account, but look over the resources to be sure what's being created. This assumes you've already got a HostedZone and a wildcard Certificate configured in your account (either automatically if this was a Sandbox account, or manually if you're running this on your own).

Setup the tools as per the Confluence page here: [Workshop 00: Setup Your AWS Environment](https://headspring.atlassian.net/wiki/spaces/WISDOM/pages/1607794769/Workshop+00%3A+Setup+Your+AWS+Environment).

We’ll exercise the following (the Workshop 00 page has all the relevant links and instructions):

* Visual Studio Code
* CDK
* Node JS / NPM
* .NET Core SDK
* AWS CLI

## Building and Deploying from Here

First, make sure you have your AWS CLI configured with the appropriate profile, and set the `AWS_DEFAULT_PROFILE` environment variable to the name of the profile on your system. If you need to setup temporary credentials for programmatic access and you're using a Headspring AWS Account, navigate to <https://headspring.awsapps.com/start>, find your account and desired role (probably "DevOps"), and click "Command line or programmatic access" and follow the on-screen instructions. Finally, set your `AWS_DEFAULT_PROFILE` environment variable to the name of the profile.

Note: SSO-based profiles don't work unless you use the "Command line or programmatic access" procedure. Only the AWS CLI knows how to transparently use the SSO to get an access key, and the CDK doesn't do that ... yet.

```pwsh
dotnet tool restore
dotnet restore
dotnet lambda package deploy/HelloWorldWorkshop/resources/HelloWorldWorkshop.zip  -pl src/HelloWorldWorkshop
cd deploy/HelloWorldWorkshop
npm install
npm run cdk -- bootstrap
```

You'll need to find three pieces of information:

The certificate to use for hosting the custom domain:

```
aws acm list-certificates
```

Copy the `CertificateArn` and the `DomainName`

```
aws route53 list-hosted-zones
```

Copy the `Id` without the `/hostedzone` part.

Finally, run the deployment (replace the `(Text)` with the values you just retrieved, make sure to remove the parentheses):

```
npm run cdk -- deploy --require-approval never --parameters "RootDomainName=(the DomainName from the Certificate)" --parameters "HostedZoneId=(The Id from the Hosted Zone)" --parameters "CertificateArn=(the CertificateArn from the Certificate)"
```

Wait for the deployment to complete, then access your new API: <https://yourdomainname.com/hello/Your%20Name%20Here>.
